package com.zyllem.sohailaziz.zyllemtest.data;

/**
 * Created by Sohail Aziz on 6/29/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class LoadFormResponse {

    @SerializedName("form")
    @Expose
    private Form form;


    @Override
    public String toString() {
        return "{" +
                "form=" + form +
                '}';
    }

    /**
     * @return The form
     */
    public Form getForm() {
        return form;
    }

    /**
     * @param form The form
     */
    public void setForm(Form form) {
        this.form = form;
    }

    /**
     * Form class
     */
    public static class Form {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("fields")
        @Expose
        private java.util.List<Field> fields = new ArrayList<Field>();
        @SerializedName("lists")
        @Expose
        private java.util.List<List> lists = new ArrayList<List>();
        @SerializedName("radios")
        @Expose
        private java.util.List<Radio> radios = new ArrayList<Radio>();
        @SerializedName("buttons")
        @Expose
        private java.util.List<Button> buttons = new ArrayList<Button>();


        @Override
        public String toString() {
            return "{" +
                    "title='" + title + '\'' +
                    ", fields=" + fields +
                    ", lists=" + lists +
                    ", radios=" + radios +
                    ", buttons=" + buttons +
                    '}';
        }

        /**
         * @return The title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @param title The title
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * @return The fields
         */
        public java.util.List<Field> getFields() {
            return fields;
        }

        /**
         * @param fields The fields
         */
        public void setFields(java.util.List<Field> fields) {
            this.fields = fields;
        }

        /**
         * @return The lists
         */
        public java.util.List<List> getLists() {
            return lists;
        }

        /**
         * @param lists The lists
         */
        public void setLists(java.util.List<List> lists) {
            this.lists = lists;
        }

        /**
         * @return The radios
         */
        public java.util.List<Radio> getRadios() {
            return radios;
        }

        /**
         * @param radios The radios
         */
        public void setRadios(java.util.List<Radio> radios) {
            this.radios = radios;
        }

        /**
         * @return The buttons
         */
        public java.util.List<Button> getButtons() {
            return buttons;
        }

        /**
         * @param buttons The buttons
         */
        public void setButtons(java.util.List<Button> buttons) {
            this.buttons = buttons;
        }

        /**
         * Form fields/widgets
         */

        /**
         * Buttons pojo
         */
        public static class Button {

            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("title")
            @Expose
            private String title;

            @Override
            public String toString() {
                return "{" +
                        "type='" + type + '\'' +
                        ", title='" + title + '\'' +
                        '}';
            }

            /**
             * @return The type
             */
            public String getType() {
                return type;
            }

            /**
             * @param type The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             * @return The title
             */
            public String getTitle() {
                return title;
            }

            /**
             * @param title The title
             */
            public void setTitle(String title) {
                this.title = title;
            }

        }

        /**
         * Edit text fields pojo
         */
        public static class Field {

            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("limit")
            @Expose
            private Integer limit;
            @SerializedName("value")
            @Expose
            private String value;

            @Override
            public String toString() {
                return "{" +
                        "type='" + type + '\'' +
                        ", title='" + title + '\'' +
                        ", limit=" + limit +
                        ", value='" + value + '\'' +
                        '}';
            }

            /**
             * @return The type
             */
            public String getType() {
                return type;
            }

            /**
             * @param type The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             * @return The title
             */
            public String getTitle() {
                return title;
            }

            /**
             * @param title The title
             */
            public void setTitle(String title) {
                this.title = title;
            }

            /**
             * @return The limit
             */
            public Integer getLimit() {
                return limit;
            }

            /**
             * @param limit The limit
             */
            public void setLimit(Integer limit) {
                this.limit = limit;
            }

            /**
             * @return The value
             */
            public String getValue() {
                return value;
            }

            /**
             * @param value The value
             */
            public void setValue(String value) {
                this.value = value;
            }

        }

        /**
         * Lists pojo
         */
        public static class List {

            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("value")
            @Expose
            private String value;
            @SerializedName("valuesList")
            @Expose
            private java.util.List<String> valuesList = new ArrayList<String>();

            @Override
            public String toString() {
                return "{" +
                        "type='" + type + '\'' +
                        ", title='" + title + '\'' +
                        ", value='" + value + '\'' +
                        ", valuesList=" + valuesList +
                        '}';
            }

            /**
             * @return The type
             */
            public String getType() {
                return type;
            }

            /**
             * @param type The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             * @return The title
             */
            public String getTitle() {
                return title;
            }

            /**
             * @param title The title
             */
            public void setTitle(String title) {
                this.title = title;
            }

            /**
             * @return The value
             */
            public String getValue() {
                return value;
            }

            /**
             * @param value The value
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * @return The valuesList
             */
            public java.util.List<String> getValuesList() {
                return valuesList;
            }

            /**
             * @param valuesList The valuesList
             */
            public void setValuesList(java.util.List<String> valuesList) {
                this.valuesList = valuesList;
            }

        }

        /**
         * Radio buttons pojo
         */
        public static class Radio {

            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("value")
            @Expose
            private String value;
            @SerializedName("valuesList")
            @Expose
            private java.util.List<String> valuesList = new ArrayList<String>();

            @Override
            public String toString() {
                return "{" +
                        "type='" + type + '\'' +
                        ", title='" + title + '\'' +
                        ", value='" + value + '\'' +
                        ", valuesList=" + valuesList +
                        '}';
            }

            /**
             * @return The type
             */
            public String getType() {
                return type;
            }

            /**
             * @param type The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             * @return The title
             */
            public String getTitle() {
                return title;
            }

            /**
             * @param title The title
             */
            public void setTitle(String title) {
                this.title = title;
            }

            /**
             * @return The value
             */
            public String getValue() {
                return value;
            }

            /**
             * @param value The value
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * @return The valuesList
             */
            public java.util.List<java.lang.String> getValuesList() {
                return valuesList;
            }

            /**
             * @param valuesList The valuesList
             */
            public void setValuesList(java.util.List<String> valuesList) {
                this.valuesList = valuesList;
            }

        }
    }

}

