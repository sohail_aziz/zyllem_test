package com.zyllem.sohailaziz.zyllemtest.presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zyllem.sohailaziz.zyllemtest.R;
import com.zyllem.sohailaziz.zyllemtest.data.LoadFormResponse;
import com.zyllem.sohailaziz.zyllemtest.data.RepositoryImpl;
import com.zyllem.sohailaziz.zyllemtest.data.WebAPI;
import com.zyllem.sohailaziz.zyllemtest.data.WebResponse;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity implements Presenter.View {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Base container
     */
    private LinearLayout linearLayoutConainter;
    private ProgressBar progressBarLoading;

    List<EditText> editTextList;
    List<Spinner> spinnerList;
    List<RadioGroup> radioGroupList;

    /**
     * Form reference for Printing purpose
     */
    private LoadFormResponse.Form form;
    /**
     * MVP Presenter for Form
     */
    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayoutConainter = (LinearLayout) findViewById(R.id.linear_layout_container);
        progressBarLoading = (ProgressBar) findViewById(R.id.progressbar);


        presenter = new Presenter(new RepositoryImpl(), this);
        presenter.loadForm();
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.setView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unSetView();
    }


    private void updateUI(LoadFormResponse body) {

        linearLayoutConainter.removeAllViews();

        //Keep the for printing later
        form = body.getForm();


        //Add Form TITLE
        TextView textViewTitle = new TextView(this);
        textViewTitle.setText(form.getTitle());
        textViewTitle.setGravity(Gravity.CENTER_HORIZONTAL);

        linearLayoutConainter.addView(textViewTitle);


        //Add ALL Edit Text FIELDS

        editTextList = new ArrayList<>(form.getFields().size());
        for (LoadFormResponse.Form.Field field : form.getFields()) {

            //get editText
            EditText editTextField = getEditText(field);
            //ADD editText

            linearLayoutConainter.addView(editTextField);
            editTextList.add(editTextField);
        }

        //Add all DropDowns/Spinners
        spinnerList = new ArrayList<>(form.getLists().size());
        for (LoadFormResponse.Form.List formList : form.getLists()) {

            //add spinner title
            TextView textViewSpinnerTitle = new TextView(this);
            textViewSpinnerTitle.setText(formList.getTitle());
            linearLayoutConainter.addView(textViewSpinnerTitle);

            //get dropdown add dropdown
            Spinner spinner = getDropdown(formList);
            linearLayoutConainter.addView(spinner);
            spinnerList.add(spinner);

        }

        //Add all radioGroups
        radioGroupList = new ArrayList<>(form.getRadios().size());
        for (LoadFormResponse.Form.Radio radio : form.getRadios()) {

            //add radio group title
            TextView textViewRadioTitle = new TextView(this);
            textViewRadioTitle.setText(radio.getTitle());
            linearLayoutConainter.addView(textViewRadioTitle);

            if (radio.getType().equals("RADIO")) {
                //add radio groups
                RadioGroup radioGroup = getRadioGroup(radio);
                linearLayoutConainter.addView(radioGroup);
                radioGroupList.add(radioGroup);
            }


        }

        //Add All BUTTONS
        for (final LoadFormResponse.Form.Button button : form.getButtons()) {

            Button b = new Button(this);
            b.setText(button.getTitle());

            final String actionType = button.getType();
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (actionType.equals("DONE")) {
                        //print();
                        print2();
                    } else if (actionType.equals("CANCEL")) {
                        closeApp();
                    } else if (actionType.equals("RESET")) {
                        clear();
                    }
                }
            });

            linearLayoutConainter.addView(b);

        }

    }


    /**
     * clears EditText, Reset DropDowns and RadioGroups
     */
    private void clear() {

        showToast("Resetting Form Fields");

        int children = linearLayoutConainter.getChildCount();

        for (int i = 0; i < children; i++) {

            View child = linearLayoutConainter.getChildAt(i);

            //clear editTexts
            if (child instanceof EditText) {

                ((EditText) child).setText("");
            }

            //reset dropDowns , make first item selected
            if (child instanceof Spinner) {

                ((Spinner) child).setSelection(0);
            }

            //reset radio groups (make first button checked
            if (child instanceof RadioGroup) {

                RadioButton radioButton = (RadioButton) ((RadioGroup) child).getChildAt(0);
                if (radioButton != null) {
                    radioButton.setChecked(true);
                }
            }
        }
    }

    /**
     * Closes App
     */
    private void closeApp() {


        showToast("Closing App");
        MainActivity.this.finish();

    }

    /**
     * Prints Form Json Array with values populated from Views
     */
    private void print() {


        assert (form != null);


        List<LoadFormResponse.Form.Field> fieldList = form.getFields();
        List<LoadFormResponse.Form.List> spinnerList = form.getLists();
        List<LoadFormResponse.Form.Radio> radioList = form.getRadios();

        int editFieldIndex = 0;
        int spinnerIndex = 0;
        int radioGroupIndex = 0;
        for (int i = 0, size = linearLayoutConainter.getChildCount(); i < size; i++) {

            View child = linearLayoutConainter.getChildAt(i);

            //get editText values and set to Form.Field
            if (child instanceof EditText) {
                EditText editTextChild = (EditText) child;
                fieldList.get(editFieldIndex).setValue(editTextChild.getText().toString());
                editFieldIndex++;
            }

            //get drop down values and set to Form.Lists
            if (child instanceof Spinner) {

                Spinner spinnerChild = (Spinner) child;
                spinnerList.get(spinnerIndex).setValue(spinnerChild.getSelectedItem().toString());
                spinnerIndex++;
            }

            //get radioGroups selection and set to Form.Radio
            if (child instanceof RadioGroup) {

                RadioGroup radioGroup = (RadioGroup) child;
                int checkedId = radioGroup.getCheckedRadioButtonId();
                RadioButton checkedButton = (RadioButton) radioGroup.findViewById(checkedId);

                //only if form List type is RADIO
                if (radioList.get(radioGroupIndex).getType().equals("RADIO")) {
                    radioList.get(radioGroupIndex).setValue(checkedButton.getText().toString());
                }
                radioGroupIndex++;
            }

        }


        Log.d(TAG, "Form Json=" + form.toString());

        Intent displayActivity = new Intent(this, DisplayActivity.class);
        displayActivity.putExtra(DisplayActivity.KEY_EXTRA, form.toString());
        startActivity(displayActivity);

    }

    /**
     * Print via saved Views
     */
    private void print2() {

        List<LoadFormResponse.Form.Field> fieldList = form.getFields();
        List<LoadFormResponse.Form.List> lList = form.getLists();
        List<LoadFormResponse.Form.Radio> radioList = form.getRadios();

        //populate fields from EditTexts
        int fIndex = 0;
        for (LoadFormResponse.Form.Field f : fieldList) {

            String value = editTextList.get(fIndex).getText().toString();
            f.setValue(value);
            fIndex++;
        }

        //populate from spinners
        int sIndex = 0;
        for (LoadFormResponse.Form.List l : lList) {

            String value = spinnerList.get(sIndex).getSelectedItem().toString();
            l.setValue(value);
            sIndex++;

        }

        //populate from radioGroups
        int rIndex = 0;
        for (LoadFormResponse.Form.Radio r : radioList) {

            RadioGroup radioGroup = radioGroupList.get(rIndex);
            int checkedId = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = (RadioButton) radioGroup.findViewById(checkedId);

            r.setValue(radioButton.getText().toString());

            rIndex++;

        }

        Log.d(TAG, "Form Json=" + form.toString());

        Intent displayActivity = new Intent(this, DisplayActivity.class);
        displayActivity.putExtra(DisplayActivity.KEY_EXTRA, form.toString());
        startActivity(displayActivity);

    }


    @Override
    public void renderForm(LoadFormResponse response) {

        updateUI(response);

        Log.d(TAG, "renderForm: LoadFormResponse=" + response.toString());

    }

    @Override
    public void renderError(String error) {
        showToast(error);
    }

    @Override
    public void showLoading() {

        //show progress bar
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {

        //hide progress bar
        progressBarLoading.setVisibility(View.GONE);
    }

    /**
     * Create and Sets and EditText with info in Field
     *
     * @param field {link Field}
     * @retur new created editText
     */
    private EditText getEditText(LoadFormResponse.Form.Field field) {

        EditText editTextField = new EditText(this);

        //set hint
        editTextField.setHint(field.getTitle());

        //set input type
        final String type = field.getType();
        if (type.equals("TEXT")) {
            editTextField.setInputType(InputType.TYPE_CLASS_TEXT);
        } else if (type.equals("EMAIL")) {
            editTextField.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        } else if (type.equals("PASSWORD")) {
            editTextField.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else if (type.equals("PHONE")) {
            editTextField.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        //set limit
        Integer limit = field.getLimit();
        if (limit != null) {
            InputFilter[] inputFilters = new InputFilter[1];
            inputFilters[0] = new InputFilter.LengthFilter(limit);
            editTextField.setFilters(inputFilters);
        }


        //set value if any
        if (field.getValue() != null) {
            editTextField.setText(field.getValue());
        }

        return editTextField;
    }

    /**
     * Creates and set dropdown based on the formList
     *
     * @param formList form.formList
     * @return newly created dropdown/Spinner
     */
    private Spinner getDropdown(LoadFormResponse.Form.List formList) {

        Spinner spinner = new Spinner(this);

        List<String> valueList = formList.getValuesList();
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, valueList);
        spinner.setAdapter(adapter);

        final String selectedValue = formList.getValue();
        for (int i = 0, size = valueList.size(); i < size; i++) {
            if (selectedValue.equals(valueList.get(i))) {
                spinner.setSelection(i);
            }
        }


        return spinner;
    }

    /**
     * Creates and populates a RadioGroup
     *
     * @param radio form.radio
     * @return RadioGroup
     */
    private RadioGroup getRadioGroup(LoadFormResponse.Form.Radio radio) {

        RadioGroup radioGroup = new RadioGroup(this);
        radioGroup.setOrientation(LinearLayout.VERTICAL);


        //add radio buttons to radioGroup
        for (String s : radio.getValuesList()) {

            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(s);
            radioGroup.addView(radioButton);

            //set checked if this is value in Form.List
            if (s.equals(radio.getValue())) {
                radioButton.setChecked(true);
            }

        }

        return radioGroup;
    }
}
