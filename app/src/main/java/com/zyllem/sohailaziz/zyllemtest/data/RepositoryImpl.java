package com.zyllem.sohailaziz.zyllemtest.data;

import android.support.annotation.NonNull;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Repository containting API calls
 * <p/>
 * Created by Sohail Aziz on 6/25/2016.
 */
public class RepositoryImpl implements Repository {

    private static final String TAG = RepositoryImpl.class.getSimpleName();
    private final WebAPI webApi = WebClient.getAPIClient();


    @Override
    public void loadForm(final LoadFormCallback callback) {

        Call<WebResponse> call = webApi.getForm();

        call.enqueue(new Callback<WebResponse>() {
            @Override
            public void onResponse(Call<WebResponse> call, Response<WebResponse> response) {

                if (response.isSuccessful()) {

                    Log.d(TAG, "response=" + response.body().toString());

                    callback.onFormLoaded(response.body());
                } else {

                    callback.onFailure(new Exception(response.errorBody().toString()));
                }
            }

            @Override
            public void onFailure(Call<WebResponse> call, Throwable t) {

                Log.e(TAG, "error=" + t.getMessage());
                callback.onFailure(new Exception(t));
            }
        });
    }


    /**
     * Calls REST API to getForm2 details
     *
     * @param callback {LoadFormV2Callback}
     */
    @Override
    public void loadFormV2(@NonNull final LoadFormV2Callback callback) {

        Call<LoadFormResponse> call = webApi.getFormV2();

        call.enqueue(new Callback<LoadFormResponse>() {
            @Override
            public void onResponse(Call<LoadFormResponse> call, Response<LoadFormResponse> response) {
                Log.d(TAG, "loadFormV2: onResponse.isSuccessful" + response.isSuccessful());

                if (response.isSuccessful()) {
                    callback.onFormV2Loaded(response.body());
                } else {
                    callback.onFailure(new Exception(response.errorBody().toString()));
                }
            }

            @Override
            public void onFailure(Call<LoadFormResponse> call, Throwable t) {

                Log.d(TAG, "loadFormV2: onFailure: t=" + t.getMessage());
                callback.onFailure(new Exception(t));
            }
        });

    }
}
