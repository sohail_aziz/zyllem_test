package com.zyllem.sohailaziz.zyllemtest.presentation;

import android.support.annotation.NonNull;

import com.zyllem.sohailaziz.zyllemtest.data.LoadFormResponse;
import com.zyllem.sohailaziz.zyllemtest.data.Repository;
import com.zyllem.sohailaziz.zyllemtest.data.WebResponse;

/**
 * Created by Sohail Aziz on 6/25/2016.
 */
public class Presenter {

    private final Repository repository;
    private View mainView;

    interface View {
        void renderForm(LoadFormResponse response);

        void renderError(String error);

        void showLoading();

        void hideLoading();
    }

    public Presenter(@NonNull Repository repository, @NonNull View view) {
        if (repository == null || view == null) {
            throw new IllegalArgumentException("repository and/or view is missing");
        }

        this.repository = repository;
        this.mainView = view;
    }

    //set view on Resume
    public void setView(View view) {
        this.mainView = view;
    }

    //unset view onPause
    public void unSetView() {
        this.mainView = null;
    }

    public void loadForm() {

        this.mainView.showLoading();
        this.repository.loadFormV2(callback);
    }

    private final Repository.LoadFormV2Callback callback = new Repository.LoadFormV2Callback() {
        @Override
        public void onFormV2Loaded(LoadFormResponse response) {

            if (mainView != null) {
                mainView.hideLoading();
                mainView.renderForm(response);
            }

        }

        @Override
        public void onFailure(Exception e) {

            if (mainView != null) {
                mainView.hideLoading();
                mainView.renderError(e.getMessage());
            }

        }
    };
}
