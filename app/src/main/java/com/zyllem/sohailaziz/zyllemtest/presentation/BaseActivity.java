package com.zyllem.sohailaziz.zyllemtest.presentation;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Base activity containing common functions
 * Created by Sohail Aziz on 6/25/2016.
 */
public class BaseActivity extends AppCompatActivity {

    void showToast(String message) {
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
