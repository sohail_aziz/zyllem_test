package com.zyllem.sohailaziz.zyllemtest.data;

/**
 * Repository containting API calls
 * Created by Sohail Aziz on 6/25/2016.
 */
public interface Repository {

    /**
     * Callback for LoadForm
     */
    interface LoadFormCallback {
        void onFormLoaded(WebResponse response);

        void onFailure(Exception e);
    }

    /**
     * Calls Rest API to get form details
     *
     * @param callback callback to indicate success/failure
     */
    void loadForm(LoadFormCallback callback);


    /**
     * Callback for loadFormV2
     */
    interface LoadFormV2Callback {
        void onFormV2Loaded(LoadFormResponse response);

        void onFailure(Exception e);
    }

    /**
     * Calls REST API to getForm2 details
     *
     * @param callback {LoadFormV2Callback}
     */
    void loadFormV2(LoadFormV2Callback callback);


}
