package com.zyllem.sohailaziz.zyllemtest.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.zyllem.sohailaziz.zyllemtest.R;

public class DisplayActivity extends AppCompatActivity {

    public static final String KEY_EXTRA = "TEXT_FIELDS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        TextView textViewFields = (TextView) findViewById(R.id.text_view_fields);

        if (getIntent().hasExtra(KEY_EXTRA)) {

            String fieldsString = getIntent().getStringExtra(KEY_EXTRA);
            textViewFields.setText(fieldsString);
        }

    }
}
