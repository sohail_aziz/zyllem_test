package com.zyllem.sohailaziz.zyllemtest.data;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Sohail Aziz on 6/25/2016.
 */
public interface WebAPI {

    /**
     * GET call to load Json Form
     * @return
     */
    @GET("/bins/3um1j")
    Call<WebResponse> getForm();

    @GET("/bins/5152t")
    Call<LoadFormResponse> getFormV2();

}
