package com.zyllem.sohailaziz.zyllemtest.data;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


/**
 * POJO response of Load Form
 */
public class WebResponse {

    @SerializedName("form")
    @Expose
    private Form form;


    @Override
    public String toString() {
        return "WebResponse{" +
                "form=" + form +
                '}';
    }

    /**
     * @return The form
     */
    public Form getForm() {
        return form;
    }

    /**
     * @param form The form
     */
    public void setForm(Form form) {
        this.form = form;
    }

    public static class Form {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("fields")
        @Expose
        private List<Field> fields = new ArrayList<Field>();
        @SerializedName("buttons")
        @Expose
        private List<Button> buttons = new ArrayList<Button>();


        @Override
        public String toString() {
            return "Form{" +
                    "title='" + title + '\'' +
                    ", fields=" + fields +
                    ", buttons=" + buttons +
                    '}';
        }

        /**
         * @return The title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @param title The title
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * @return The fields
         */
        public List<Field> getFields() {
            return fields;
        }

        /**
         * @param fields The fields
         */
        public void setFields(List<Field> fields) {
            this.fields = fields;
        }

        /**
         * @return The buttons
         */
        public List<Button> getButtons() {
            return buttons;
        }

        /**
         * @param buttons The buttons
         */
        public void setButtons(List<Button> buttons) {
            this.buttons = buttons;
        }

        public static class Field {

            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("limit")
            @Expose
            private Integer limit;
            @SerializedName("value")
            @Expose
            private String value;


            @Override
            public String toString() {
                return "{" +
                        "type='" + type + '\'' +
                        ", title='" + title + '\'' +
                        ", limit=" + limit +
                        ", value='" + value + '\'' +
                        '}';
            }

            /**
             * @return The type
             */
            public String getType() {
                return type;
            }

            /**
             * @param type The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             * @return The title
             */
            public String getTitle() {
                return title;
            }

            /**
             * @param title The title
             */
            public void setTitle(String title) {
                this.title = title;
            }

            /**
             * @return The limit
             */
            public Integer getLimit() {
                return limit;
            }

            /**
             * @param limit The limit
             */
            public void setLimit(Integer limit) {
                this.limit = limit;
            }

            /**
             * @return The value
             */
            public String getValue() {
                return value;
            }

            /**
             * @param value The value
             */
            public void setValue(String value) {
                this.value = value;
            }

        }

        public static class Button {

            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("title")
            @Expose
            private String title;

            @Override
            public String toString() {
                return "Button{" +
                        "type='" + type + '\'' +
                        ", title='" + title + '\'' +
                        '}';
            }

            /**
             * @return The type
             */
            public String getType() {
                return type;
            }

            /**
             * @param type The type
             */
            public void setType(String type) {
                this.type = type;
            }

            /**
             * @return The title
             */
            public String getTitle() {
                return title;
            }

            /**
             * @param title The title
             */
            public void setTitle(String title) {
                this.title = title;
            }

        }


    }
}






