package com.zyllem.sohailaziz.zyllemtest.presentation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sohail Aziz on 6/25/2016.
 */
@Deprecated
public class PFields implements Parcelable {

    private String type;
    private String title;
    private Integer limit;
    private String value;

    public PFields() {
    }

    @Override
    public String toString() {
        return "PFields{" +
                "type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", limit=" + limit +
                ", value='" + value + '\'' +
                '}';
    }

    protected PFields(Parcel in) {
        type = in.readString();
        title = in.readString();
        limit = in.readByte() == 0x00 ? null : in.readInt();
        value = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(title);
        if (limit == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(limit);
        }
        dest.writeString(value);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PFields> CREATOR = new Parcelable.Creator<PFields>() {
        @Override
        public PFields createFromParcel(Parcel in) {
            return new PFields(in);
        }

        @Override
        public PFields[] newArray(int size) {
            return new PFields[size];
        }
    };
}
